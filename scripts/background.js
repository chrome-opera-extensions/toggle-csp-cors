const handleResponse = (res) => {
     let { responseHeaders } = res;

     responseHeaders = responseHeaders.filter((header) => {
          //&& header.name.toLowerCase() !== "x-frame-options"
          return (header.name.toLowerCase() !== "access-control-allow-origin" && header.name.toLowerCase() !== "content-security-policy");
     });

     responseHeaders.push({
          name: 'Access-Control-Allow-Origin',
          value: '*'
     });

     responseHeaders.push({
          name: 'Access-Control-Allow-Credentials',
          value: 'true'
     });

     //window.res = responseHeaders;
     //console.log("responseHeaders:", responseHeaders);
     return { responseHeaders };
}

const setOn = () => {
     chrome.browserAction.setBadgeText({
          text: 'on'
     });

     chrome.browserAction.setBadgeBackgroundColor({
          color: [86, 171, 86, 1]
     });

     chrome.webRequest.onHeadersReceived.addListener(handleResponse, {
          urls: ["*://*/*", "<all_urls>"],
          types: ["main_frame", "sub_frame", "xmlhttprequest"]
     }, ["blocking", "responseHeaders"]);
};

const setOff = () => {
     chrome.browserAction.setBadgeText({
          text: 'off'
     });

     chrome.browserAction.setBadgeBackgroundColor({
          color: [145, 145, 145, 1]
     });
     
     chrome.webRequest.onHeadersReceived.removeListener(
          handleResponse
     );
};

if (localStorage.getItem('on')) {
     setOn();
}
else {
     setOff();
}

chrome.browserAction.onClicked.addListener(() => {
     if (localStorage.getItem('on')) {
          localStorage.setItem('on', '');
          setOff();
     }
     else {
          localStorage.setItem('on', '1');
          setOn();
     }
});
