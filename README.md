Toggle CORS\CSP
---
[![License: MIT](https://img.shields.io/badge/License-MIT-blue.svg?style=plastic)](https://gitlab.com/chrome-opera-extensions/toggle-csp-cors/blob/master/LICENSE)

![version](https://img.shields.io/badge/version-1.0.0-blue.svg?style=plastic)

Author: Richard Abel<br>
Date Created: 7/18/2019<br>

This extension disables and enables (toggles) CORS and CSP, setting CSP to null and CORS to the * wildcard.